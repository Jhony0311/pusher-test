import React from 'react';

const Actions = (props) => {
  if (props.viewType === 'biz') {
    return (
      <div>
        <button onClick={(args) => props.sendMsg('REQUEST')}>Request</button>
        <button onClick={(args) => props.sendMsg('DONE')}>Done</button>
      </div>
    )
  }
  return (
    <div>
      <button onClick={(args) => props.sendMsg('ACCEPT')}>Accept</button>
      <button onClick={(args) => props.sendMsg('DENY')}>Deny</button>
      <button onClick={(args) => props.sendMsg('DONE')}>Done</button>
    </div>
  )
};

export default Actions;

import React, { Component } from 'react';
import './App.css';

// Components
import Menu from './Menu';
import Thread from './Thread';

class App extends Component {
  constructor(props) {
    super(props);
    this.state = {
      viewType: 'biz',
    };
    this.changeTo = this.changeTo.bind(this);
  }

  changeTo(type) {
    if (type !== 'biz' && type !== 'free') {
      return true;
    }
    this.setState(() => {
      return {
        viewType: type
      }
    });
  }

  render() {
    return (
      <div className="App">
        <div className="App-header">
          <Menu {...this.state} changeTo={this.changeTo}  />
          <h2>{`Viewing as a ${this.state.viewType}`}</h2>
        </div>
        <div className="App-intro">
          <Thread {...this.state} />
        </div>
      </div>
    );
  }
}

export default App;

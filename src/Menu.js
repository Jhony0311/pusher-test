import React from 'react';

const Menu = (props) => {
  return (
    <nav>
      <button onClick={() => props.changeTo('free')}>Freelance View</button>
      <button onClick={() => props.changeTo('biz')}>Business View</button>
    </nav>
  );
};

export default Menu;

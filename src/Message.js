import React from 'react';

const Message = ({event, message}) => {
  console.log(event, message);
  return (
    <div className="message">
      <p className="type">Type: <span>{event}</span></p>
      <p className="text">Message: <span>{message}</span></p>
    </div>
  )
};

export default Message;

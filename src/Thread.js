import React, { Component } from 'react';
import Pusher from 'pusher-js';

import Actions from './Actions';
import Message from './Message';

class Thread extends Component {
  constructor(props) {
    super(props);
    this.state = {
      messages: []
    }
    this.pusherSetup = this.pusherSetup.bind(this);
    this.sendMsg = this.sendMsg.bind(this);
  }

  componentDidMount() {
    this.pusherSetup();
  }

  shouldComponentUpdate(nextProps, nextState) {
    if (nextProps.viewType !== this.props.viewType) {
      return true;
    }
    if (nextState.messages !== this.state.messages) {
      return true;
    }
    return false;
  }

  pusherSetup() {
    Pusher.logToConsole = true;
    const pusher = new Pusher(process.env.REACT_APP_PUSHER_KEY, {
      encrypted: true
    });
    this.channel = pusher.subscribe('tickets-thread', {
      encrypted: true
    });

    // bind events to state
    this.channel.bind('REQUEST', this.evHandler, this);
    this.channel.bind('client-ACCEPT', this.evHandler, this);
    this.channel.bind('client-DENY', this.evHandler, this);
    this.channel.bind('client-DONE', this.evHandler, this);
  }

  evHandler(data) {
    switch (data.event) {
      case 'REQUEST':
      case 'ACCEPT':
      case 'DENY':
      case 'DONE':
        this.setState((prevState) => {
          return {
            messages: [
              ...prevState.messages,
              data
            ]
          };
        });
        break;
      default:
        alert('undefined action request');
        return true;
    }
  }

  sendMsg(action) {
    switch (action) {
      case 'REQUEST':
        this.channel.trigger('REQUEST', {
          event: 'REQUEST',
          message: 'Hey there, this is a new request for you!'
        });
        break;
      case 'ACCEPT':
        this.channel.trigger('client-ACCEPT', {
          event: 'REQUEST',
          message: 'Guess what?, Someone had accepted your request'
        });
        break;
      case 'DENY':
        this.channel.trigger('client-DENY', {
          event: 'DENY',
          message: 'Bad news, Someone had denied your request. Try again!'
        });
        break;
      case 'DONE':
        this.channel.trigger('client-DONE', {
          event: 'DONE',
          message: 'Yei!, the request is marked as done!'
        });
        break;
      default:
        alert('undefined action request');
        return true;
    }
  }

  render() {
    const {viewType} = this.props;
    const {messages} = this.state;
    console.log(messages.length);
    return (
      <div className="wrapper">
        <div className="thread">
          {messages.length > 0 &&
            messages.map((m, i) => <Message {...m} key={i} />)
          }
        </div>
        <div className="actions">
          <Actions viewType={viewType} sendMsg={this.sendMsg} />
        </div>
      </div>
    );
  }
}

export default Thread;
